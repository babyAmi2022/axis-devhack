package live.faceauth.axisbank;

import android.app.Application;
import com.facebook.stetho.Stetho;
import com.squareup.picasso.Picasso;
import live.faceauth.sdk.FaceAuth;

public class AxisDemoApplication extends Application {

  public void onCreate() {
    super.onCreate();
    Stetho.initializeWithDefaults(this);
    FaceAuth.getInstance().initialize(this);
    final Picasso picasso = new Picasso.Builder(this).build();
    Picasso.setSingletonInstance(picasso);
  }
}
