package live.faceauth.axisbank.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import java.util.UUID;
import live.faceauth.axisbank.R;
import live.faceauth.sdk.FaceAuth;

public class PreRegisterActivity extends AppCompatActivity {
  @BindView(R.id.name) EditText nameView;
  @BindView(R.id.phoneNumber) EditText phoneNumberView;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_pre_register);
    ButterKnife.bind(this);
  }

  @OnClick(R.id.signInWrapper) public void onSignInClick(View v) {
    startActivity(new Intent(this, PreAuthenticateActivity.class));
    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    finish();
  }

  @OnClick(R.id.registerButton) public void onRegisterClick(View v) {
    boolean skip = false;
    String name = nameView.getText().toString();
    String phoneNumber = phoneNumberView.getText().toString();
    if (validateInput(name, phoneNumber)) {
      Intent intent = new Intent();
      intent.putExtra("NAME", name);
      intent.putExtra("PHONE_NUMBER", phoneNumber);
      intent.putExtra("IS_KYC_FLOW", false);
      if (skip) {
        openPreAuthActivity(phoneNumber);
        return;
      }

      FaceAuth.getInstance().register(this, intent);
    }
  }

  private boolean validateInput(String name, String phoneNumber) {
    nameView.setError(null);
    phoneNumberView.setError(null);
    if (TextUtils.isEmpty(name)) {
      nameView.setError(getString(R.string.error_enter_name));
    }
    if (TextUtils.isEmpty(phoneNumber)) {
      phoneNumberView.setError(getString(R.string.error_enter_phone));
    }
    return !TextUtils.isEmpty(name) && !TextUtils.isEmpty(phoneNumber);
  }

  @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    final String name = nameView.getText().toString();
    final String phoneNumber = phoneNumberView.getText().toString();
    FaceAuth.getInstance()
        .handleRegistration(requestCode, resultCode, data, new FaceAuth.RegistrationCallback() {
          @Override public void onSuccess(UUID registeredFaceId, Uri imageUri) {
            openPreAuthActivity(phoneNumber);
          }

          @Override public void onError(Exception e) {
            String message = getString(R.string.error_reg_failed) + "\n" + e.getMessage();
            showAuthResultDialog(message, false);
          }
        });

    super.onActivityResult(requestCode, resultCode, data);
  }

  private void showAuthResultDialog(String message, final boolean success) {
    new AlertDialog.Builder(this).setMessage(message)
        .setTitle(R.string.err_reg_failed)
        .setPositiveButton("OK", null)
        .show();
  }

  public void openPreAuthActivity(final String phoneNumber) {
    new AlertDialog.Builder(this).setTitle(R.string.reg_success)
        .setMessage(R.string.reg_success_msg)
        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
          @Override public void onClick(DialogInterface dialog, int which) {
            Intent intent = new Intent(PreRegisterActivity.this, PreAuthenticateActivity.class);
            intent.putExtra("PHONE_NUMBER", phoneNumber);
            startActivity(intent);
            finish();
          }
        })
        .setCancelable(false)
        .show();
  }
}
