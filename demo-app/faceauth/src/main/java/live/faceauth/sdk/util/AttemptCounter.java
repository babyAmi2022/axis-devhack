package live.faceauth.sdk.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class AttemptCounter {
  public static final String ATTEMPT_COUNTER = "ATTEMPT_COUNTER";
  public static final String ATTEMPT_COUNT_KEY = "ATTEMPT_COUNT";
  public static final String FIRST_ATTEMPT_TIME_KEY = "FIRST_ATTEMPT_TIME";

  private static SharedPreferences get(Context context) {
    return context.getSharedPreferences(ATTEMPT_COUNTER, Context.MODE_PRIVATE);
  }

  public static int getAttemptCount(Context context) {
    return get(context).getInt(ATTEMPT_COUNT_KEY, 0);
  }

  public static void setAttemptCount(Context context, int count) {
    get(context).edit().putInt(ATTEMPT_COUNT_KEY, count).apply();
  }

  public static long getFirstAttemptTime(Context context) {
    return get(context).getLong(FIRST_ATTEMPT_TIME_KEY, 0);
  }

  public static void logFirstAttemptTime(Context context) {
    get(context).edit().putLong(FIRST_ATTEMPT_TIME_KEY, System.currentTimeMillis()).apply();
  }

  public static void resetAttemptCounter(Context context) {
    get(context).edit().clear().apply();
  }

  public static boolean canAttempt(Context context) {
    return System.currentTimeMillis()
        > getFirstAttemptTime(context) + Settings.getWrongAttemptsTimeout(context);
  }

  public static boolean countAttempt(Context context) {
    int count = getAttemptCount(context);
    Log.d("AttemptCounter", "Counting attempt after:" + count);
    if (count == 0) {
      setAttemptCount(context, 1);
      logFirstAttemptTime(context);
    } else if (count < Settings.getWrongAttemptsLimit(context)) {
      setAttemptCount(context, count + 1);
    } else if (canAttempt(context)) {
      resetAttemptCounter(context);
      countAttempt(context);
    } else {
      return false;
    }
    return true;
  }
}
