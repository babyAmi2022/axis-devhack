package live.faceauth.sdk;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;
import java.util.UUID;
import live.faceauth.sdk.network.ApiHelper;
import live.faceauth.sdk.ui.AuthenticateActivity;
import live.faceauth.sdk.ui.GestureAuthenticateActivity;
import live.faceauth.sdk.ui.KycDocCaptureActivity;
import live.faceauth.sdk.ui.PermissionsActivity;
import live.faceauth.sdk.ui.RegisterActivity;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FaceAuth {

  public static final int REGISTER_FACE_REQUEST = 5001;
  public static final int AUTHENTICATE_FACE_REQUEST = 5002;
  public static final int UPLOAD_KYC_DOC_REQUEST = 5003;

  public static final String REGISTERED_FACE_ID = "uuid_faceid_result";
  public static final String AUTO_CLICK_MODE = "auto_click_mode";
  public static final String AUTH_RESULT = "auth_result";
  public static final String AUTH_CONFIDENCE_RESULT = "auth_confidence_result";
  public static final String AUTH_SPOOF_SCORE = "auth_spoof_score";

  private static final String FACEAUTH_API_KEY = "faceauth-api-key";
  private static final String FACEAUTH_APP_NAME = "faceauth-app-name";

  private static final Object mLock = new Object();
  private static final String TAG = "FaceAuth";
  private static FaceAuth sInstance;

  private FaceAuthConfig mConfig;

  private String mApiKey;
  private String mAppName;

  @NonNull public static FaceAuth getInstance() {
    if (sInstance == null) {
      synchronized (mLock) {
        sInstance = new FaceAuth();
      }
    }
    return sInstance;
  }

  private static String readApiKey(Context context) {
    try {
      final ApplicationInfo ai = context.getPackageManager()
          .getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);

      return ai.metaData.getString(FACEAUTH_API_KEY);
    } catch (PackageManager.NameNotFoundException e) {
      Log.e(TAG, "Failed to load meta-data, NameNotFound: " + e.getMessage());
    } catch (NullPointerException e) {
      Log.e(TAG, "Failed to load meta-data, NullPointer: " + e.getMessage());
    }
    return "";
  }

  private static String readAppName(Context context) {
    try {
      final ApplicationInfo ai = context.getPackageManager()
          .getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);

      return ai.metaData.getString(FACEAUTH_APP_NAME);
    } catch (PackageManager.NameNotFoundException e) {
      Log.e(TAG, "Failed to load meta-data, NameNotFound: " + e.getMessage());
    } catch (NullPointerException e) {
      Log.e(TAG, "Failed to load meta-data, NullPointer: " + e.getMessage());
    }
    return "";
  }

  private FaceAuth() {
    this.mConfig = new FaceAuthConfig();
  }

  /**
   * Provide the endpoint and key for connecting to Azure
   */
  public void initialize(Context context) {
    final String apiKey = readApiKey(context);
    final String appName = readAppName(context);

    if ((apiKey != null && !apiKey.isEmpty()) && (appName != null && !appName.isEmpty())) {
      Log.d(TAG, "FaceAuth initialization successful.");
    } else {
      Log.d(TAG,
          "Failed to read api and app key. Make sure to add it in Android Manifest as meta-data.");
    }
    this.mApiKey = apiKey;
    this.mAppName = appName;
  }

  /**
   * Modify the configuration (like threshold, messages etc.)
   *
   * @param config The configuration to be used.
   */
  public void configure(FaceAuthConfig config) {
    this.mConfig = config;
  }

  /**
   * Get the current configuration
   */
  public static FaceAuthConfig getConfig() {
    return getInstance().mConfig;
  }

  /**
   * Invoke the face registration flow.
   *
   * @param caller The calling activity
   */
  public void register(Activity caller, Intent src) {
    Intent registerIntent = new Intent(caller, RegisterActivity.class);
    if (src != null) registerIntent.putExtras(src);
    Intent intent = new Intent(caller, PermissionsActivity.class);
    intent.putExtra(Intent.EXTRA_INTENT, registerIntent);
    caller.startActivityForResult(intent, REGISTER_FACE_REQUEST);
  }

  public void register(Activity caller) {
    register(caller, null);
  }

  /**
   * Invoke the face registration flow.
   *
   * @param caller The calling activity
   */
  public void uploadKycDoc(Activity caller, Intent src) {
    Intent registerIntent = new Intent(caller, KycDocCaptureActivity.class);
    if (src != null) registerIntent.putExtras(src);
    Intent intent = new Intent(caller, PermissionsActivity.class);
    intent.putExtra(Intent.EXTRA_INTENT, registerIntent);
    caller.startActivityForResult(intent, UPLOAD_KYC_DOC_REQUEST);
  }

  public void uploadKycDoc(Activity caller) {
    uploadKycDoc(caller, null);
  }

  /**
   * Invoke the face authentication flow.
   *
   * @param caller The calling activity
   * @param autoMode Auto click image and try to verify
   */
  public void authenticateLegacy(Activity caller, String phoneNumber, boolean autoMode) {
    Intent authIntent = new Intent(caller, AuthenticateActivity.class);
    authIntent.putExtra("PHONE_NUMBER", phoneNumber);
    authIntent.putExtra(AUTO_CLICK_MODE, autoMode);

    Intent intent = new Intent(caller, PermissionsActivity.class);
    intent.putExtra(Intent.EXTRA_INTENT, authIntent);
    // TODO (aakashns): Set the necessary parameters
    caller.startActivityForResult(intent, AUTHENTICATE_FACE_REQUEST);
  }

  public void authenticateWithGestures(Activity caller, String phoneNumber, boolean autoMode) {
    Intent authIntent = new Intent(caller, GestureAuthenticateActivity.class);
    authIntent.putExtra("PHONE_NUMBER", phoneNumber);
    authIntent.putExtra(AUTO_CLICK_MODE, autoMode);

    Intent intent = new Intent(caller, PermissionsActivity.class);
    intent.putExtra(Intent.EXTRA_INTENT, authIntent);
    // TODO (aakashns): Set the necessary parameters
    caller.startActivityForResult(intent, AUTHENTICATE_FACE_REQUEST);
  }

  public void authenticate(Activity caller, String registeredFaceId, boolean autoMode) {
    Intent authIntent = new Intent(caller, AuthenticateActivity.class);
    authIntent.putExtra(REGISTERED_FACE_ID, registeredFaceId);
    authIntent.putExtra(AUTO_CLICK_MODE, autoMode);

    Intent intent = new Intent(caller, PermissionsActivity.class);
    intent.putExtra(Intent.EXTRA_INTENT, authIntent);
    // TODO (aakashns): Set the necessary parameters
    caller.startActivityForResult(intent, AUTHENTICATE_FACE_REQUEST);
  }

  /**
   * Handle the result from register in onActivityResult
   */
  public boolean handleUploadKycDoc(int requestCode, int resultCode, Intent result,
      RegistrationCallback callback) {
    if (requestCode == UPLOAD_KYC_DOC_REQUEST) {
      if (resultCode == Activity.RESULT_OK) {
        String faceId = result.getStringExtra(FaceAuth.REGISTERED_FACE_ID);
        callback.onSuccess(UUID.fromString(faceId), result.getData());
      } else {
        callback.onError(new Exception("Something went wrong"));
      }
      return true;
    }
    return false;
  }

  /**
   * Handle the result from register in onActivityResult
   */
  public boolean handleRegistration(int requestCode, int resultCode, Intent result,
      RegistrationCallback callback) {
    if (requestCode == REGISTER_FACE_REQUEST) {
      if (resultCode == Activity.RESULT_OK) {
        String faceId = result.getStringExtra(FaceAuth.REGISTERED_FACE_ID);
        callback.onSuccess(UUID.fromString(faceId), result.getData());
      } else {
        callback.onError(new Exception("Something went wrong"));
      }
      return true;
    }
    return false;
  }

  // TODO : Use the right kind of request here
  public boolean handleKycPhotoCapture(int requestCode, int resultCode, Intent result,
      RegistrationCallback callback) {
    if (requestCode == REGISTER_FACE_REQUEST) {
      if (resultCode == Activity.RESULT_OK) {
        String faceId = result.getStringExtra(FaceAuth.REGISTERED_FACE_ID);
        callback.onSuccess(UUID.fromString(faceId), result.getData());
      } else {
        callback.onError(new Exception("Something went wrong"));
      }
      return true;
    }
    return false;
  }

  public boolean handleKycDocumentCapture(int requestCode, int resultCode, Intent result,
      RegistrationCallback callback) {
    if (requestCode == REGISTER_FACE_REQUEST) {
      if (resultCode == Activity.RESULT_OK) {
        String faceId = result.getStringExtra(FaceAuth.REGISTERED_FACE_ID);
        callback.onSuccess(UUID.fromString(faceId), result.getData());
      } else {
        callback.onError(new Exception("Something went wrong"));
      }
      return true;
    }
    return false;
  }

  /**
   * Handle the result from the authenticate in onActivityResult
   */
  public boolean handleAuthentication(int requestCode, int resultCode, Intent result,
      AuthenticationCallback callback) {
    if (requestCode == AUTHENTICATE_FACE_REQUEST) {
      if (resultCode == Activity.RESULT_OK) {
        boolean isIdentical = result.getBooleanExtra(AUTH_RESULT, false);
        double confidence = result.getDoubleExtra(AUTH_CONFIDENCE_RESULT, 0.0f);
        double realConfidence = result.getDoubleExtra(AUTH_SPOOF_SCORE, 0.0f);
        String name = result.getStringExtra("NAME");
        String phoneNumber = result.getStringExtra("PHONE_NUMBER");
        String requestId = result.getStringExtra("REQUEST_ID");
        String imageUrl = result.getStringExtra("PROFILE_PICTURE");
        int conf = (int) (confidence * 100);
        double scr = (int) (realConfidence * 100);
        if (isIdentical) {
          callback.onSuccess(conf, scr, name, phoneNumber, requestId, imageUrl);
        } else {
          callback.onFailure(conf, scr);
        }
      } else {
        callback.onError(new Exception("Something went wrong"));
      }
      return true;
    }

    return false;
  }

  public void sendFeedback(String requestId, int rating, String feedback,
      final FeedbackCallback callback) {

    ApiHelper.sendFeedback(requestId, rating, feedback, new Callback<ResponseBody>() {
      @Override public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
        callback.onSuccess();
      }

      @Override public void onFailure(Call<ResponseBody> call, Throwable t) {
        callback.onFailure(t.getMessage());
      }
    });
  }

  public String getApiKey() {
    return mApiKey;
  }

  public String getAppName() {
    return mAppName;
  }

  public void captureKycPhoto(Activity activity) {
    // TODO: Open RegisterActivity
  }

  public void captureKycDocumentPhoto(Activity activity) {
    // TODO: Open RegisterKycDocumentPhoto
  }

  /**
   * Callbacks for registration
   */
  public interface RegistrationCallback {

    /**
     * The face was successfully registered and a face ID was returned.
     */
    void onSuccess(UUID registeredFaceId, Uri imageUri);

    /**
     * Something went wrong while trying to register the face, or the user cancelled
     */
    void onError(Exception e);
  }

  /**
   * Callbacks for authentication
   */
  public interface AuthenticationCallback {

    /**
     * The authentication was successful
     */
    void onSuccess(int confidence, double score, String name, String phoneNumber, String requestId,
        String imageUrl);

    /**
     * The authentication was unsuccessful
     */
    void onFailure(int confidence, double score);

    /**
     * Something went wrong while trying to authenticate the face, or the user cancelled
     */
    void onError(Exception e);
  }

  public interface FeedbackCallback {
    void onSuccess();

    void onFailure(String message);
  }
}
