package live.faceauth.sdk.gesture;

import android.hardware.SensorEvent;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import com.google.android.gms.vision.face.Face;

public abstract class GestureTask {

  static String SMILE_TASK = "SmileTask";
  static String BLINK_TASK = "BlinkTask";
  static String FINISH_TASK = "FinishTask";
  static String PAUSE_TASK = "PauseTask";
  static String SHAKING_TASK = "ShakingTask";
  static String NODDING_TASK = "NoddingTask";

  protected long mTrackingTimeStart;
  protected long mTrackingTimeEnd;
  protected boolean mFaceSmall = false;

  private String mNegativeTask = null;

  public void startTracking() {
    mTrackingTimeStart = System.currentTimeMillis();
    android.util.Log.d("GestureTask", "start: " + getTaskName());
  }

  public void stopTracking() {
    mTrackingTimeEnd = System.currentTimeMillis();

    android.util.Log.d("GestureTask", "stop: " + getTaskName());

    log();
  }

  abstract String getTaskName();

  public void setFaceSmall(boolean tooSmall) {
    mFaceSmall = tooSmall;
  }

  abstract void updateFace(Face face);

  void updateSensorEvent(SensorEvent event) {
  }

  protected boolean isFaceLargeEnough() {
    return !mFaceSmall;
  }

  abstract boolean isLiveFace();

  void setNegativeTask(String negativeTask) {
    mNegativeTask = negativeTask;
  }

  @StringRes abstract int getHintMessageId(boolean tooSmall);

  @DrawableRes public int getSuggestionDrawable() {
    return -1;
  }

  @Nullable String getDebugMessage() {
    if (mNegativeTask != null) {
      return "Please don't " + mNegativeTask;
    } else {
      return null;
    }
  }

  abstract void log();

  enum TYPE {
    SMILE, BLINK, NODDING, SHAKE
  }
}
