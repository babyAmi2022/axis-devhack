package live.faceauth.sdk.util;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

public class CrashlyticsWrapper {

  public static void logException(Throwable throwable) {
    // This check makes sure to log only when parent project has configured Crashlytics
    if (Fabric.isInitialized()) {
      Crashlytics.logException(throwable);
      android.util.Log.d("CrashlyticsWrapper", "logged:" + throwable.getMessage());
    } else {
      android.util.Log.d("CrashlyticsWrapper", "not initialized");
    }
  }
}
