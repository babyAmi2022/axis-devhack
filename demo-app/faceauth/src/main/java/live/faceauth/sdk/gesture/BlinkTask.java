package live.faceauth.sdk.gesture;

import android.support.annotation.Nullable;
import com.google.android.gms.vision.face.Face;
import live.faceauth.sdk.R;

public class BlinkTask extends GestureTask {

  private static final String LOG_TAG = "BlinkTask";

  private static final float BLINK_PROB_THRESHOLD = 0.2f;
  private static final float EYES_CLOSE_PROB = 0.6f;

  private float mMaxLeftEyeOpenProbability = 0.0f;
  private float mMinLeftEyeOpenProbability = 1.0f;

  private float mMaxRightEyeOpenProbability = 0.0f;
  private float mMinRightEyeOpenProbability = 1.0f;

  @Override String getTaskName() {
    return BLINK_TASK;
  }

  @Override public void updateFace(Face face) {

    float leftEyeOpenProb = face.getIsLeftEyeOpenProbability();
    float rightEyeOpenProb = face.getIsRightEyeOpenProbability();

    if (leftEyeOpenProb >= 0) {
      mMinLeftEyeOpenProbability = Math.min(mMinLeftEyeOpenProbability, leftEyeOpenProb);
      mMaxLeftEyeOpenProbability = Math.max(mMaxLeftEyeOpenProbability, leftEyeOpenProb);
    }

    if (rightEyeOpenProb >= 0) {
      mMinRightEyeOpenProbability = Math.min(mMinRightEyeOpenProbability, rightEyeOpenProb);
      mMaxRightEyeOpenProbability = Math.max(mMaxRightEyeOpenProbability, rightEyeOpenProb);
    }
  }

  private float getLeftEyeOpenProbDiff() {
    return mMaxLeftEyeOpenProbability - mMinLeftEyeOpenProbability;
  }

  private boolean hasLeftEyeBlinked() {
    //return mMaxLeftEyeOpenProbability >= 0.7f && mMinLeftEyeOpenProbability <= 0.5f;
    return getLeftEyeOpenProbDiff() >= BLINK_PROB_THRESHOLD
        && mMinLeftEyeOpenProbability <= EYES_CLOSE_PROB;
  }

  private boolean hasRightEyeBlinked() {
    //return mMaxRightEyeOpenProbability >= 0.7f && mMinRightEyeOpenProbability <= 0.5f;
    return getRightEyeOpenProbDiff() >= BLINK_PROB_THRESHOLD
        && mMinRightEyeOpenProbability <= EYES_CLOSE_PROB;
  }

  private float getRightEyeOpenProbDiff() {
    return mMaxRightEyeOpenProbability - mMinRightEyeOpenProbability;
  }

  private boolean hasBothEyesBlinked() {
    return hasLeftEyeBlinked() && hasRightEyeBlinked();
  }

  private boolean hasBlinked() {
    return hasBothEyesBlinked();
  }

  @Override public boolean isLiveFace() {
    return hasBlinked() && isFaceLargeEnough();
  }

  @Override public int getHintMessageId(boolean tooSmall) {
    if (!isFaceLargeEnough()) {
      return R.string.tracker_face_too_small;
    } else {
      return R.string.tracker_blink;
    }
  }

  @Override public int getSuggestionDrawable() {
    return R.drawable.blink_anim;
  }

  @Nullable String getDebugMessage() {
    return "Left: "
        + mMinLeftEyeOpenProbability
        + ","
        + mMaxLeftEyeOpenProbability
        + "\n"
        + "Right: "
        + mMinRightEyeOpenProbability
        + ","
        + mMaxRightEyeOpenProbability;
  }

  @Override public void log() {
    long duration = mTrackingTimeEnd - mTrackingTimeStart;
    float leftEyeDiff = getLeftEyeOpenProbDiff();
    float rightEyeDiff = getRightEyeOpenProbDiff();

    android.util.Log.d(LOG_TAG, "Tracked between: " + mTrackingTimeStart + "," + mTrackingTimeEnd);
    android.util.Log.d(LOG_TAG, "Tracked duration: " + duration);

    android.util.Log.d(LOG_TAG,
        "Left eye open prob: " + mMinLeftEyeOpenProbability + "," + mMaxLeftEyeOpenProbability);
    android.util.Log.d(LOG_TAG, "Left eye open diff: " + leftEyeDiff);

    android.util.Log.d(LOG_TAG,
        "Right eye open prob: " + mMinRightEyeOpenProbability + "," + mMaxRightEyeOpenProbability);
    android.util.Log.d(LOG_TAG, "Right eye open diff: " + rightEyeDiff);
  }
}
