package live.faceauth.sdk.network;

import java.util.UUID;
import live.faceauth.sdk.models.RegisterResponse;
import live.faceauth.sdk.models.SubmitKycResponse;
import live.faceauth.sdk.models.VerifyResponse;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface FaceAuthService {

  @POST("register") Call<RegisterResponse> register(@Body RequestBody body);

  @POST("verify") Call<VerifyResponse> verify(@Header("x-registered-face-id") UUID registeredFaceId,
      @Body RequestBody body);

  @POST("register_sbi") Call<RegisterResponse> registerWithNamePhone(@Header("x-name") String name,
      @Header("x-phone-number") String phoneNumber, @Body RequestBody body);

  @POST("verify_sbi") Call<VerifyResponse> verifyWithPhone(
      @Header("x-phone-number") String phoneNumber, @Body RequestBody body);

  @POST("feedback_sbi") Call<ResponseBody> sendFeedback(@Header("x-request-id") String requestId,
      @Header("x-rating") int rating, @Header("x-feedback") String feedback);

  @POST("upload_kyc_photo") Call<RegisterResponse> uploadKycPhoto(@Body RequestBody body);

  @POST("upload_kyc_doc") Call<RegisterResponse> uploadKycDoc(@Body RequestBody body);

  @POST("submit_kyc") Call<SubmitKycResponse> submitKyc(@Header("x-name") String name,
      @Header("x-phone-number") String phoneNumber, @Header("x-faceid-1") String faceId,
      @Header("x-docid-1") String docId);
}
