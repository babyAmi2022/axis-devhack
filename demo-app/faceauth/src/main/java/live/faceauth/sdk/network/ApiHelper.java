package live.faceauth.sdk.network;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import live.faceauth.sdk.FaceAuth;
import live.faceauth.sdk.models.RegisterResponse;
import live.faceauth.sdk.models.SubmitKycResponse;
import live.faceauth.sdk.models.VerifyResponse;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiHelper {

  //private static final String BASE_URL = "https://faceauth.live/api/";
  //private static final String BASE_URL = "http://ec2-52-66-66-46.ap-south-1.compute.amazonaws.com:3000/api/v1/";
  //private static final String BASE_URL = "http://144.21.82.138:3000/api/v1/";

  private static FaceAuthService service() {

    final OkHttpClient okHttpClient = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
      @Override public Response intercept(Chain chain) throws IOException {
        Request.Builder request = chain.request().newBuilder();
        request.addHeader("x-faceauth-api-key", FaceAuth.getInstance().getApiKey())
            .addHeader("Connection", "keep-alive")
            .addHeader("x-app-key", FaceAuth.getInstance().getAppName())
            .addHeader("Content-Type", "image/png");

        return chain.proceed(request.build());
      }
    }).connectTimeout(30, TimeUnit.SECONDS).readTimeout(30, TimeUnit.SECONDS).build();

    final Retrofit retrofit = new Retrofit.Builder().client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl(FaceAuth.getConfig().endpoint)
        .build();

    return retrofit.create(FaceAuthService.class);
  }

  public static void register(InputStream is, Callback<RegisterResponse> callback) {
    service().register(getResponseBody(is)).enqueue(callback);
  }

  public static void registerNamePhone(String name, String phoneNumber, InputStream is,
      Callback<RegisterResponse> callback) {
    service().registerWithNamePhone(name, phoneNumber, getResponseBody(is)).enqueue(callback);
  }

  public static void uploadKycPhoto(InputStream is, Callback<RegisterResponse> callback) {
    service().uploadKycPhoto(getResponseBody(is)).enqueue(callback);
  }

  public static void uploadKycDoc(InputStream is, Callback<RegisterResponse> callback) {
    service().uploadKycDoc(getResponseBody(is)).enqueue(callback);
  }

  public static void submitKyc(String name, String phoneNumber, String faceId, String docId,
      Callback<SubmitKycResponse> callback) {
    service().submitKyc(name, phoneNumber, faceId, docId).enqueue(callback);
  }

  public static void verifySBI(String phoneNumber, InputStream is,
      Callback<VerifyResponse> callback) {
    service().verifyWithPhone(phoneNumber, getResponseBody(is)).enqueue(callback);
  }

  public static void verify(UUID registeredFaceId, InputStream is,
      Callback<VerifyResponse> callback) {
    service().verify(registeredFaceId, getResponseBody(is)).enqueue(callback);
  }

  public static void sendFeedback(String requestId, int rating, String feedback,
      Callback<ResponseBody> callback) {
    service().sendFeedback(requestId, rating, feedback).enqueue(callback);
  }

  private static RequestBody getResponseBody(InputStream is) {
    final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    final byte[] bytes = new byte[1024];
    int bytesRead;

    try {
      while ((bytesRead = is.read(bytes)) > 0) {
        byteArrayOutputStream.write(bytes, 0, bytesRead);
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    return RequestBody.create(MediaType.parse("image/png"), byteArrayOutputStream.toByteArray());
  }
}
