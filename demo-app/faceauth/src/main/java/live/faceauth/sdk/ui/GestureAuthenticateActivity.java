package live.faceauth.sdk.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.common.images.Size;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.FaceDetector;
import java.io.InputStream;
import java.util.List;
import live.faceauth.sdk.FaceAuth;
import live.faceauth.sdk.FaceAuthConfig;
import live.faceauth.sdk.R;
import live.faceauth.sdk.gesture.GestureController;
import live.faceauth.sdk.models.MatchResponse;
import live.faceauth.sdk.models.SpoofResponse;
import live.faceauth.sdk.models.VerifyResponse;
import live.faceauth.sdk.network.ApiHelper;
import live.faceauth.sdk.ui.camera.CameraSource;
import live.faceauth.sdk.ui.camera.CameraSourcePreview;
import live.faceauth.sdk.ui.camera.GraphicOverlay;
import live.faceauth.sdk.util.AttemptCounter;
import live.faceauth.sdk.util.CameraManager;
import live.faceauth.sdk.util.CrashlyticsWrapper;
import live.faceauth.sdk.util.FaceInfo;
import live.faceauth.sdk.util.ImageUtil;
import live.faceauth.sdk.util.PermissionUtil;
import live.faceauth.sdk.util.Settings;
import pl.droidsonroids.gif.GifImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class GestureAuthenticateActivity extends AppCompatActivity
    implements View.OnClickListener, CameraManager.PictureClickListener, SensorEventListener {

  private static final String TAG = "AuthenticateActivity";
  private static final boolean IS_DEBUG = false;

  private GraphicOverlay mGraphicOverlay;
  private ProgressDialog mProgressDialog;
  private CameraManager mCameraManager;
  //private UUID mRegisteredFaceId;
  private TextView mTopMessaging;
  private TextView mTopDebugMessaging;
  private GifImageView mGifDrawable;

  private boolean mProcessing = false;
  private boolean mAutoClickMode = false;
  private SparseArray<FaceInfo> mFaceInfo = new SparseArray<>();
  private FaceDetector detector;
  int mSuggestionDrawableId = -1;

  @Override protected void onCreate(Bundle savedInstanceState) {
    detector = new FaceDetector.Builder(this).setTrackingEnabled(false)
        .setLandmarkType(FaceDetector.NO_LANDMARKS)
        .build();
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_gesture_authenticate);

    mProgressDialog = new ProgressDialog(this);
    mProgressDialog.setCancelable(false);
    mProgressDialog.setMessage("Processing...");

    CameraSourcePreview preview = findViewById(R.id.preview);
    mGraphicOverlay = findViewById(R.id.faceOverlay);
    ImageButton captureButton = findViewById(R.id.button_capture);
    mTopMessaging = findViewById(R.id.top_messaging);
    mTopDebugMessaging = findViewById(R.id.top_debug_messaging);
    mGifDrawable = findViewById(R.id.gesture_demo_view);

    SensorManager sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

    if (sensorManager != null) {
      Sensor accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
      sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
      Log.d("Accelerometer", "sensor attached");
    }

    captureButton.setOnClickListener(this);
    try {
      findViewById(R.id.toggle_camera).setOnClickListener(this);
    } catch (Exception e) {
      Log.e("AuthenticateActivity", "Download");
    }

    //mRegisteredFaceId = UUID.fromString(getIntent().getStringExtra(FaceAuth.REGISTERED_FACE_ID));

    mAutoClickMode = Settings.authOnSmile(this);

    if (mAutoClickMode) {
      captureButton.setVisibility(View.GONE);
    } else {
      captureButton.setVisibility(View.VISIBLE);
    }

    // Switch to back camera if front camera doesn't exist
    int cameraSourceId = CameraSource.CAMERA_FACING_FRONT;

    if (!CameraSource.hasRequestedCamera(cameraSourceId)) {
      Toast.makeText(this, getString(R.string.front_camera_error), Toast.LENGTH_LONG).show();
      CrashlyticsWrapper.logException(new Exception("Could not find requested camera."));
      finish();
      return;
    }

    mCameraManager =
        new CameraManager(this, mGraphicOverlay, getWindowManager(), preview, cameraSourceId, this);

    mCameraManager.setFaceDetectorMode(FaceDetector.ACCURATE_MODE);

    if (!FaceAuth.getConfig().authenticationCameraSound) {
      // TODO (siddhant): Disable the camera sound here.
    }

    // Check for the camera permission before accessing the camera.  If the
    // permission is not granted yet, request permission.
    List<String> permissions =
        PermissionUtil.checkMissingPermissions(this, CAMERA, WRITE_EXTERNAL_STORAGE,
            READ_EXTERNAL_STORAGE);
    if (permissions.isEmpty()) {
      mCameraManager.createCameraSource();
    } else {
      finish();
    }

    if (Settings.getAuthCameraToggle(this)) {
      findViewById(R.id.toggle_camera).setVisibility(View.VISIBLE);
    } else {
      findViewById(R.id.toggle_camera).setVisibility(View.GONE);
    }
  }

  private void showProgress() {
    //mProgressDialog.show();
  }

  private void hideProgress() {
    mProgressDialog.hide();
  }

  @Override protected void onResume() {
    super.onResume();
    mCameraManager.onResume();
  }

  @Override protected void onPause() {
    super.onPause();
    mCameraManager.onPause();
  }

  @Override protected void onDestroy() {
    if (mCameraManager != null) {
      mCameraManager.onDestroy();
    }
    super.onDestroy();
  }

  private void captureImage() {
    showProgress();
    mCameraManager.captureImage();
  }

  @Override public void onClick(View view) {
    if (view.getId() == R.id.button_capture) {
      final List<String> requestPermissions =
          PermissionUtil.checkMissingPermissions(this, CAMERA, WRITE_EXTERNAL_STORAGE,
              READ_EXTERNAL_STORAGE);

      if (requestPermissions.isEmpty()) {
        captureImage();
      } else {
        finish();
      }
    } else if (view.getId() == R.id.toggle_camera) {
      mCameraManager.toggleCamera();
    }
  }

  @Override public void onPictureClick(byte[] bytes) {
    Log.d(TAG, "onPictureClick");
    processAuthentication(bytes);
  }

  @Override public void onPictureSave(Uri uri) {
  }

  final Runnable mUpdateFaceRunnable = new Runnable() {
    @Override public void run() {
      boolean checkAttemptLimit = Settings.shouldLimitAttempts(GestureAuthenticateActivity.this);
      boolean checkLargeEnough = true;
      boolean tooSmall = false;
      boolean isFaceLargeEnough;
      if (mAutoClickMode) {

        if (mFaceInfo.size() == 1 && !mProcessing) {
          // capture image

          FaceInfo faceInfo = mFaceInfo.valueAt(0);
          if (faceInfo.getResetOnCheck()) {
            faceInfo.reset();
          }

          isFaceLargeEnough = !checkLargeEnough || isLargeEnough(faceInfo);
          tooSmall = !isFaceLargeEnough;
          GestureController.get().setFaceSmall(tooSmall);

          if (GestureController.get().hasFinished()) {
            if (!checkAttemptLimit || AttemptCounter.countAttempt(
                GestureAuthenticateActivity.this)) {

              if (isFaceLargeEnough) {
                captureImage();
                mProcessing = true;
                GestureController.get().reset();
                faceInfo.setResetOnCheck();
              }
            } else {
              showAttemptsWarning();
              mCameraManager.stopPreview();
              return;
            }
          }
        }

        final int suggestionDrawableId = GestureController.get().getSuggestionDrawable();

        if (suggestionDrawableId != -1) {
          if (suggestionDrawableId != mSuggestionDrawableId) {
            mGifDrawable.setBackgroundResource(suggestionDrawableId);
          }
          mSuggestionDrawableId = suggestionDrawableId;
          mGifDrawable.setVisibility(View.VISIBLE);
        } else {
          mGifDrawable.setVisibility(View.GONE);
        }

        //final int messageId = FaceInfo.getHintMessageId(mFaceInfo, mProcessing, tooSmall);
        final int messageId = GestureController.get().getHintMessageId(tooSmall);
        if (!mProcessing) {
          mTopMessaging.setText(messageId);
        } else {
          mTopMessaging.setText(R.string.processing);
        }

        final String debugMessage = GestureController.get().getDebugMessage();
        if (debugMessage == null || !IS_DEBUG) {
          mTopDebugMessaging.setVisibility(View.GONE);
        } else {
          mTopDebugMessaging.setVisibility(View.VISIBLE);
          mTopDebugMessaging.setText(debugMessage);
        }
      }
    }
  };

  private void showAttemptsWarning() {
    long duration = 1
        + (Settings.getWrongAttemptsTimeout(this) - System.currentTimeMillis()
        + AttemptCounter.getFirstAttemptTime(this)) / 60000;
    new AlertDialog.Builder(this).setTitle("Too Many Attempts")
        .setMessage("You have exceeded the maximum number of failed attempts. Please try after "
            + duration
            + " minute(s).")
        .setCancelable(false)
        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
          @Override public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
            finish();
          }
        })
        .show();
  }

  private boolean isPortraitMode() {
    int orientation = getResources().getConfiguration().orientation;
    if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
      return false;
    }
    if (orientation == Configuration.ORIENTATION_PORTRAIT) {
      return true;
    }

    return false;
  }

  private boolean isLargeEnough(FaceInfo faceInfo) {
    final Size size = mCameraManager.getPreviewSize();
    int min = Math.min(size.getWidth(), size.getHeight());
    int max = Math.max(size.getWidth(), size.getHeight());
    final float imageWidth;
    final float imageHeight;
    if (isPortraitMode()) {
      imageWidth = min;
      imageHeight = max;
    } else {
      imageWidth = max;
      imageHeight = min;
    }

    double ratio = faceInfo.height / (imageHeight + 0.000001);
    Log.d("AuthAct", "ratio" + ratio);

    return faceInfo.height > imageHeight * 0.45;
  }

  private boolean isLargeEnoughLegacy(FaceInfo faceInfo) {
    DisplayMetrics displayMetrics = new DisplayMetrics();
    mCameraManager.mWindowManager.getDefaultDisplay().getMetrics(displayMetrics);
    return faceInfo.height > displayMetrics.heightPixels * 0.3;
  }

  @Override @WorkerThread public void updateFaces(final SparseArray<FaceInfo> faceInfo) {
    if (mAutoClickMode) {
      mFaceInfo = faceInfo;
      runOnUiThread(mUpdateFaceRunnable);
    }
  }

  private void processAuthentication(byte[] data) {
    final Bitmap bitmap = ImageUtil.getBitmap(data);

    final Frame frame = new Frame.Builder().setBitmap(bitmap).build();
    final SparseArray<Face> faces = detector.detect(frame);

    final Bitmap finalBitmap = ImageUtil.getCroppedFace(bitmap, faces);
    //final Bitmap finalBitmap = ImageUtil.getScaledBitmap(bitmap, 320);
    final InputStream finalBitmapStream = ImageUtil.getBitmapInputStream(finalBitmap);

    bitmap.recycle();
    finalBitmap.recycle();

    String phoneNumber = getIntent().getStringExtra("PHONE_NUMBER");
    verify(phoneNumber, finalBitmapStream);
  }

  private void verify(String phoneNumber, InputStream stream) {
    ApiHelper.verifySBI(phoneNumber, stream, new Callback<VerifyResponse>() {
      @Override
      public void onResponse(Call<VerifyResponse> call, Response<VerifyResponse> response) {
        Log.d("verify", "Got a response from verify API");
        if (response.isSuccessful()
            && response.body().success
            && response.body().match.success
            && response.body().spoof.success) {

          final VerifyResponse verifyResponse = response.body();
          handleAuthResult(verifyResponse.match, verifyResponse.spoof,
              verifyResponse.match.personal.name, verifyResponse.match.personal.phoneNumber,
              verifyResponse.requestId, verifyResponse.match.personal.image_url);

          Log.d("verify", "success");
        } else {
          Toast.makeText(GestureAuthenticateActivity.this,
              "Verification unsuccessful! Please try again.", Toast.LENGTH_SHORT).show();
          Log.d("verify", "unsuccessful");
          finishProcessing();
        }
      }

      @Override public void onFailure(Call<VerifyResponse> call, Throwable t) {
        Log.e("verify", "failure", t);
        Toast.makeText(GestureAuthenticateActivity.this, "Request timed out. Please try again!",
            Toast.LENGTH_LONG).show();
        finishProcessing();
      }
    });
  }

  private void finishProcessing() {
    mCameraManager.resetCamera();
    hideProgress();
    mProcessing = false;
  }

  private void handleAuthResult(@NonNull MatchResponse matchResponse,
      @NonNull SpoofResponse spoofResponse, final String name, final String phoneNumber,
      final String requestId, final String imageUrl) {

    hideProgress();

    final FaceAuthConfig config = FaceAuth.getConfig();
    final boolean isSuccess = spoofResponse.confidence > Settings.getLivenessThreshold(this)
        && matchResponse.confidence > Settings.getMatchThreshold(this);

    final double matchConfidence = matchResponse.confidence;
    final double realConfidence = spoofResponse.confidence;

    if (!isSuccess && config.allowAuthRetry) {
      new AlertDialog.Builder(this).setTitle(R.string.face_auth_failed)
          .setMessage(getString(R.string.auth_failed_no_face) + " \n\n" + "Face Match: " + ((int) (
              matchConfidence
                  * 100)) + " % \n" + "Face Liveness: " + ((int) (realConfidence * 100)) + " %")
          .setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
            @Override public void onClick(DialogInterface dialogInterface, int i) {
              finishProcessing();
            }
          })
          .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override public void onClick(DialogInterface dialog, int which) {
              returnAuthResult(false, matchConfidence, realConfidence, name, phoneNumber, requestId,
                  imageUrl);
            }
          })
          .setCancelable(false)
          .show();
    } else {
      returnAuthResult(isSuccess, matchConfidence, realConfidence, name, phoneNumber, requestId,
          imageUrl);
    }
  }

  private void returnAuthResult(boolean isSuccess, double confidence, double realConfidence,
      String name, String phoneNumber, String requestId, String imageUrl) {
    if (isSuccess) {
      AttemptCounter.resetAttemptCounter(this);
    }
    Intent data = new Intent();
    data.putExtra(FaceAuth.AUTH_RESULT, isSuccess);
    data.putExtra(FaceAuth.AUTH_CONFIDENCE_RESULT, confidence);
    data.putExtra(FaceAuth.AUTH_SPOOF_SCORE, realConfidence);
    data.putExtra("NAME", name);
    data.putExtra("PHONE_NUMBER", phoneNumber);
    data.putExtra("REQUEST_ID", requestId);
    data.putExtra("PROFILE_PICTURE", imageUrl);
    setResult(RESULT_OK, data);
    finish();
  }

  @Override public void onSensorChanged(SensorEvent event) {
    final Sensor sensor = event.sensor;

    if (sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
      mCameraManager.setSensorEvent(event);
    }
  }

  @Override public void onAccuracyChanged(Sensor sensor, int accuracy) {
  }
}
