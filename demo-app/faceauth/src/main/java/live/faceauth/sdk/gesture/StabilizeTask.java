package live.faceauth.sdk.gesture;

import android.graphics.PointF;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.support.annotation.Nullable;
import android.util.Log;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.Landmark;
import java.util.List;
import live.faceauth.sdk.R;

public class StabilizeTask extends GestureTask {

  private boolean mIsMoving = false;
  private boolean mIsFaceMoving = false;
  private float mLastX, mLastY, mLastZ;

  private static final long STABLE_DURATION = 30;
  private static final long FACE_STABLE_DURATION = 110;

  private long mLastUpdate;
  private long mLastChanged;

  private long mLastFaceUpdated;
  private long mLastFaceStateChanged;

  private PointF mFacePosition = null;

  private float mFaceAbsRate = -1.0f;

  public StabilizeTask() {
    mLastChanged = System.currentTimeMillis();
    mLastUpdate = System.currentTimeMillis();

    mLastFaceStateChanged = System.currentTimeMillis();
    mLastFaceUpdated = System.currentTimeMillis();
  }

  @Override String getTaskName() {
    return PAUSE_TASK;
  }

  private float sqr(float num) {
    return num * num;
  }

  private Landmark getLandmark(List<Landmark> landmarks, int type) {
    for (Landmark landmark : landmarks) {
      if (landmark.getType() == type) {
        return landmark;
      }
    }
    return null;
  }

  private PointF getPosition(Landmark landmark) {
    if (landmark != null) {
      return landmark.getPosition();
    } else {
      return null;
    }
  }

  @Override public int getSuggestionDrawable() {
    return R.drawable.face_normal;
  }

  @Override void updateFace(Face face) {
    if (face != null) {
      long currentTime = System.currentTimeMillis();

      if ((currentTime - mLastFaceUpdated) > 10) {
        Landmark nose = getLandmark(face.getLandmarks(), Landmark.NOSE_BASE);

        //PointF facePosition = face.getPosition();
        PointF facePosition = getPosition(nose);

        long dT = currentTime - mLastFaceUpdated;

        if (mFacePosition != null && facePosition != null) {
          float absRate = (float) Math.sqrt(
              sqr(facePosition.x - mFacePosition.x) + sqr(facePosition.y - mFacePosition.y)) / dT
              * 1000;

          boolean isMoving = absRate > 200.0f;

          if (isMoving != mIsFaceMoving) {
            mLastFaceStateChanged = currentTime;
            if (!isMoving) {
              Log.d("Stabilize", "stable");
            }
          }

          mFaceAbsRate = absRate;

          //if (isMoving) {
          //  Log.d("Stabilize", "moving");
          //}

          //Log.d("Stabilize", "moving: " + absRate);
          mIsFaceMoving = isMoving;
        }

        mLastFaceUpdated = currentTime;
        mFacePosition = facePosition;
      }
    }
  }

  @Override void updateSensorEvent(SensorEvent event) {
    if (event == null) {
      return;
    }

    final Sensor sensor = event.sensor;

    if (sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
      float x = event.values[0];
      float y = event.values[1];
      float z = event.values[2];

      long currentTime = System.currentTimeMillis();

      if ((currentTime - mLastUpdate) > 20) {
        if (mLastUpdate != 0) {

          long dT = currentTime - mLastUpdate;

          float absRate = (float) Math.sqrt(
              (x - mLastX) * (x - mLastX) + (y - mLastY) * (y - mLastY) + (z - mLastZ) * (z
                  - mLastZ)) / dT * 1000;

          boolean isMoving = absRate > 4.0f;

          if (isMoving != mIsMoving) {
            mLastChanged = currentTime;
          }

          mIsMoving = isMoving;
        }

        mLastUpdate = currentTime;
        mLastX = x;
        mLastY = y;
        mLastZ = z;
      }
    }
  }

  private boolean isFaceDetectedRecently() {
    return System.currentTimeMillis() - mLastFaceUpdated < 100;
  }

  private boolean isFaceStable() {
    return (System.currentTimeMillis() - mLastFaceStateChanged > FACE_STABLE_DURATION)
        && !mIsFaceMoving;
  }

  private boolean isPhoneStable() {
    //return false;
    return (System.currentTimeMillis() - mLastChanged > STABLE_DURATION) && !mIsMoving;
  }

  private boolean isPhoneStableDebug() {
    //return false;
    return (System.currentTimeMillis() - mLastChanged > STABLE_DURATION) && !mIsMoving;
  }

  @Override boolean isLiveFace() {
    return isPhoneStable() && isFaceStable() && isFaceDetectedRecently() && isFaceLargeEnough();
  }

  @Override int getHintMessageId(boolean tooSmall) {
    if (!isFaceDetectedRecently()) {
      return R.string.tracker_no_face;
    } else if (!isFaceLargeEnough()) {
      return R.string.tracker_face_too_small;
    } else if (!isFaceStable()) {
      return R.string.tracker_stay_still_face;
    } else if (!isPhoneStableDebug()) {
      return R.string.tracker_stay_still;
    } else {
      return R.string.tracker_processing;
    }
  }

  @Nullable String getDebugMessage() {
    return "Face speed: " + mFaceAbsRate;
  }

  @Override void log() {

  }
}
