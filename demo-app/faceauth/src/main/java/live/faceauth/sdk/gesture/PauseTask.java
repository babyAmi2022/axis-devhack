package live.faceauth.sdk.gesture;

import com.google.android.gms.vision.face.Face;
import live.faceauth.sdk.R;

public class PauseTask extends GestureTask {

  private static final float PAUSE_DURATION = 1000.0f;

  private float mPauseSec = 1.0f;

  public PauseTask(float pauseSec) {
    mPauseSec = pauseSec;
  }

  public PauseTask() {
    mPauseSec = 1.0f;
  }

  @Override String getTaskName() {
    return PAUSE_TASK;
  }

  @Override void updateFace(Face face) {

  }

  @Override public int getSuggestionDrawable() {
    return R.drawable.face_normal;
  }

  @Override boolean isLiveFace() {
    return System.currentTimeMillis() - mTrackingTimeStart > mPauseSec * PAUSE_DURATION;
  }

  @Override int getHintMessageId(boolean tooSmall) {
    return R.string.tracker_processing;
  }

  @Override void log() {

  }
}
