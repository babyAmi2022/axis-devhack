package live.faceauth.sdk.models;

public class RegisterResponse {

  public final boolean success;

  public final Face face;

  public final String requestId;

  public RegisterResponse(boolean success, Face face, String requestId) {
    this.success = success;
    this.face = face;
    this.requestId = requestId;
  }
}
