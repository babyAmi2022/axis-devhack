package live.faceauth.sdk.models;

import android.support.annotation.Nullable;

public class PersonalResponse {
  public final String name;
  public final String phoneNumber;
  public String image_url;

  public PersonalResponse(String name, String phoneNumber, @Nullable String image_url) {
    this.name = name;
    this.phoneNumber = phoneNumber;
    this.image_url = image_url;
  }
}
