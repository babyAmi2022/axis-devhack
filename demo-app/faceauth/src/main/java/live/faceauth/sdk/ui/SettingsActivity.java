package live.faceauth.sdk.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;
import live.faceauth.sdk.R;
import live.faceauth.sdk.util.Settings;

public class SettingsActivity extends AppCompatActivity {

  Switch limitAttempts;
  EditText attemptLimit;
  EditText attemptTimeout;
  Switch authCameraToggle;
  EditText matchThreshold;
  EditText livenessThreshold;
  Switch gestureMode;
  Switch authOnSmile;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_settings);

    limitAttempts = findViewById(R.id.limitAttempts);
    attemptLimit = findViewById(R.id.attemptLimit);
    attemptTimeout = findViewById(R.id.attemptTimeout);
    authCameraToggle = findViewById(R.id.authCameraToggle);
    gestureMode = findViewById(R.id.gestureMode);
    matchThreshold = findViewById(R.id.matchThreshold);
    livenessThreshold = findViewById(R.id.livenessThreshold);
    authOnSmile = findViewById(R.id.authOnSmile);
    findViewById(R.id.saveButton).setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        saveSettings();
        Toast.makeText(SettingsActivity.this, "Settings saved.", Toast.LENGTH_SHORT).show();
        finish();
      }
    });

    limitAttempts.setChecked(Settings.shouldLimitAttempts(this));
    attemptLimit.setText(String.valueOf(Settings.getWrongAttemptsLimit(this)));
    attemptTimeout.setText(String.valueOf(Settings.getWrongAttemptsTimeout(this) / 60000L));
    authCameraToggle.setChecked(Settings.getAuthCameraToggle(this));
    gestureMode.setChecked(Settings.getGestureMode(this));
    matchThreshold.setText(String.valueOf(Settings.getMatchThreshold(this)));
    livenessThreshold.setText(String.valueOf(Settings.getLivenessThreshold(this)));
    authOnSmile.setChecked(Settings.authOnSmile(this));
  }

  private void saveSettings() {
    Settings.setLimitAttempts(this, limitAttempts.isChecked());
    Settings.setWrongAttemptsLimit(this, Integer.valueOf(attemptLimit.getText().toString()));
    Settings.setWrongAttemptsTimeout(this,
        Long.valueOf(attemptTimeout.getText().toString()) * 60000L);
    Settings.setAuthCameraToggle(this, authCameraToggle.isChecked());
    Settings.setMatchThreshold(this, Double.valueOf(matchThreshold.getText().toString()));
    Settings.setLivenessThreshold(this, Float.valueOf(livenessThreshold.getText().toString()));
    Settings.setGestureMode(this, gestureMode.isChecked());
    Settings.setAuthOnSmile(this, authOnSmile.isChecked());
  }
}
