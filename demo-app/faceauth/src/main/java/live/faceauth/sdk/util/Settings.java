package live.faceauth.sdk.util;

import android.content.Context;
import android.content.SharedPreferences;
import live.faceauth.sdk.FaceAuth;

public class Settings {
  public static final String SETTINGS_PREF = "SETTINGS_PREF";

  public static SharedPreferences get(Context context) {
    return context.getSharedPreferences(SETTINGS_PREF, Context.MODE_PRIVATE);
  }

  public static boolean shouldLimitAttempts(Context context) {
    return get(context).getBoolean(Keys.LIMIT_ATTEMPTS, false);
  }

  public static void setLimitAttempts(Context context, boolean shouldLimit) {
    get(context).edit().putBoolean(Keys.LIMIT_ATTEMPTS, shouldLimit).apply();
  }

  public static int getWrongAttemptsLimit(Context context) {
    return get(context).getInt(Keys.WRONG_ATTEMPTS_LIMIT, 3);
  }

  public static void setWrongAttemptsLimit(Context context, int limit) {
    get(context).edit().putInt(Keys.WRONG_ATTEMPTS_LIMIT, limit).apply();
  }

  public static long getWrongAttemptsTimeout(Context context) {
    return get(context).getLong(Keys.WRONG_ATTEMPTS_TIMEOUT, 60000);
  }

  public static void setWrongAttemptsTimeout(Context context, long millis) {
    get(context).edit().putLong(Keys.WRONG_ATTEMPTS_TIMEOUT, millis).apply();
  }

  public static boolean getAuthCameraToggle(Context context) {
    return get(context).getBoolean(Keys.AUTH_CAMERA_TOGGLE, false);
  }

  public static void setAuthCameraToggle(Context context, boolean show) {
    get(context).edit().putBoolean(Keys.AUTH_CAMERA_TOGGLE, show).apply();
  }

  public static double getMatchThreshold(Context context) {
    return FaceAuth.getConfig().matchThreshold;
  }

  public static void setMatchThreshold(Context context, double threshold) {
    FaceAuth.getConfig().matchThreshold = threshold;
  }

  public static float getLivenessThreshold(Context context) {
    return get(context).getFloat(Keys.LIVENESS_THRESHOLD, 0.6f);
  }

  public static void setLivenessThreshold(Context context, float threshold) {
    get(context).edit().putFloat(Keys.LIVENESS_THRESHOLD, threshold).apply();
  }

  public static boolean getGestureMode(Context context) {
    return get(context).getBoolean(Keys.GESTURE_MODE, true);
  }

  public static void setGestureMode(Context context, boolean enabled) {
    get(context).edit().putBoolean(Keys.GESTURE_MODE, enabled).apply();
  }

  public static boolean authOnSmile(Context context) {
    return get(context).getBoolean(Keys.SMILE_TO_AUTH, true);
  }

  public static void setAuthOnSmile(Context context, boolean enabled) {
    get(context).edit().putBoolean(Keys.SMILE_TO_AUTH, enabled).apply();
  }

  public static final class Keys {
    public static final String LIMIT_ATTEMPTS = "LIMIT_ATTEMPTS";
    public static final String WRONG_ATTEMPTS_LIMIT = "WRONG_ATTEMPTS_LIMIT";
    public static final String WRONG_ATTEMPTS_TIMEOUT = "WRONG_ATTEMPTS_TIMEOUT";
    public static final String AUTH_CAMERA_TOGGLE = "AUTH_CAMERA_TOGGLE";
    //public static final String MATCH_THRESHOLD = "MATCH_THRESHOLD";
    public static final String LIVENESS_THRESHOLD = "LIVENESS_THRESHOLD";
    public static final String SMILE_TO_AUTH = "SMILE_TO_AUTH";
    public static final String ENDPOINT = "ENDPOINT";
    public static final String GESTURE_MODE = "GESTURE_MODE";
  }
}
