package live.faceauth.sdk.util;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.net.Uri;
import android.os.Environment;
import android.support.media.ExifInterface;
import android.util.Log;
import android.util.SparseArray;
import com.google.android.gms.vision.face.Face;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ImageUtil {

  public static final int MEDIA_TYPE_IMAGE = 1;
  public static final int MEDIA_TYPE_VIDEO = 2;

  public static final String TAG = "ImageUtil";

  /** Create a file Uri for saving an image or video */
  public static Uri getOutputMediaFileUri(int type) {
    return Uri.fromFile(getOutputMediaFile(type));
  }

  /** Create a File for saving an image or video */
  public static File getOutputMediaFile(int type) {
    // To be safe, you should check that the SDCard is mounted
    // using Environment.getExternalStorageState() before doing this.

    File mediaStorageDir =
        new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
            "MyCameraApp");
    // This location works best if you want the created images to be shared
    // between applications and persist after your app has been uninstalled.

    // Create the storage directory if it does not exist
    if (!mediaStorageDir.exists()) {
      if (!mediaStorageDir.mkdirs()) {
        Log.d("MyCameraApp", "failed to create directory");
        return null;
      }
    }

    // Create a media file name
    @SuppressLint("SimpleDateFormat") String timeStamp =
        new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
    File mediaFile;
    if (type == MEDIA_TYPE_IMAGE) {
      mediaFile =
          new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");
    } else if (type == MEDIA_TYPE_VIDEO) {
      mediaFile =
          new File(mediaStorageDir.getPath() + File.separator + "VID_" + timeStamp + ".mp4");
    } else {
      return null;
    }

    return mediaFile;
  }

  public static Bitmap getScaledBitmap(byte[] data, int cutoff) {
    final InputStream stream = new ByteArrayInputStream(data);
    Bitmap bitmap = getScaledBitmap(stream, cutoff);

    try {
      stream.close();
    } catch (IOException e) {
      e.printStackTrace();
    }

    return bitmap;
  }

  public static Bitmap getScaledBitmap(InputStream stream, int cutoff) {
    final BitmapFactory.Options options = new BitmapFactory.Options();
    options.inPreferredConfig = Bitmap.Config.ARGB_8888;
    Bitmap bitmap = BitmapFactory.decodeStream(stream);

    if (bitmap.getWidth() > cutoff) {
      bitmap =
          Bitmap.createScaledBitmap(bitmap, cutoff, bitmap.getHeight() * cutoff / bitmap.getWidth(),
              false);
    }
    return bitmap;
  }

  public static Bitmap getBitmap(InputStream stream) {
    final BitmapFactory.Options options = new BitmapFactory.Options();
    options.inPreferredConfig = Bitmap.Config.ARGB_8888;

    return BitmapFactory.decodeStream(stream);
  }

  public static Bitmap getBitmap(byte[] data) {
    final InputStream stream = new ByteArrayInputStream(data);
    Bitmap bitmap = getBitmap(stream);
    try {
      stream.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return bitmap;
  }

  public static Bitmap getScaledBitmap(Bitmap bitmap, int cutoff) {
    final BitmapFactory.Options options = new BitmapFactory.Options();
    options.inPreferredConfig = Bitmap.Config.ARGB_8888;
    final Bitmap finalBitmap;
    if (bitmap.getWidth() > cutoff) {
      finalBitmap =
          Bitmap.createScaledBitmap(bitmap, cutoff, bitmap.getHeight() * cutoff / bitmap.getWidth(),
              false);
    } else {
      finalBitmap = bitmap;
    }
    return finalBitmap;
  }

  public static int getRotation(ExifInterface exifInterface) {
    int rotation = 0;
    int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION,
        ExifInterface.ORIENTATION_NORMAL);
    switch (orientation) {
      case ExifInterface.ORIENTATION_ROTATE_90:
        rotation = 90;
        break;
      case ExifInterface.ORIENTATION_ROTATE_180:
        rotation = 180;
        break;
      case ExifInterface.ORIENTATION_ROTATE_270:
        rotation = 270;
        break;
    }
    return rotation;
  }

  public static Bitmap rotateBitmap(Bitmap source, int angle) {
    Matrix matrix = new Matrix();
    matrix.postRotate(angle);
    return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
  }

  public static Bitmap getCroppedFace(Bitmap bitmap, SparseArray<Face> faces) {

    Bitmap finalBitmap;
    try {
      final Face face = faces.get(faces.keyAt(0));
      final PointF pos = face.getPosition();
      int x = (int) pos.x;
      int y = (int) pos.y;
      int width = (int) face.getWidth();
      int height = (int) face.getHeight();

      finalBitmap = Bitmap.createBitmap(bitmap, x, y, width, height);
      finalBitmap = ImageUtil.getScaledBitmap(finalBitmap, 320);
    } catch (Exception e) {
      Log.d(TAG, "Failed to crop face " + e.getMessage(), e);
      finalBitmap = ImageUtil.getScaledBitmap(bitmap, 640);
    }

    return finalBitmap;
  }

  public static InputStream getBitmapInputStream(Bitmap bitmap) {
    final ByteArrayOutputStream bos = new ByteArrayOutputStream();
    bitmap.compress(Bitmap.CompressFormat.JPEG, 99, bos);

    final InputStream finalBitmapStream = new ByteArrayInputStream(bos.toByteArray());

    try {
      bos.close();
    } catch (IOException e) {
      e.printStackTrace();
    }

    return finalBitmapStream;
  }
}
