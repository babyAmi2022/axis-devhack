package live.faceauth.sdk.ui;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

public class OvalView extends View {

  private static final double WIDTH_HEIGHT_RATIO = 0.788888889;

  public OvalView(Context context) {
    super(context);
  }

  public OvalView(Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
  }

  public OvalView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  public void onMeasure(int widthSpec, int heightSpec) {
    int originalWidth = MeasureSpec.getSize(widthSpec);

    int originalHeight = MeasureSpec.getSize(heightSpec);
    // Find out width based on fit height
    int width = (int) (WIDTH_HEIGHT_RATIO * originalHeight);

    super.onMeasure(MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY),
        MeasureSpec.makeMeasureSpec(originalHeight, MeasureSpec.EXACTLY));
  }
}
