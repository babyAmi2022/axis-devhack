package live.faceauth.sdk.models;

public class VerifyResponse {

  public final boolean success;

  public final MatchResponse match;

  public final SpoofResponse spoof;

  public final String requestId;

  public VerifyResponse(boolean success, MatchResponse match, SpoofResponse spoof,
      String requestId) {
    this.success = success;
    this.match = match;
    this.spoof = spoof;
    this.requestId = requestId;
  }
}
