package live.faceauth.sdk.models;

public class KycMatchResponse {

  public final boolean success;

  public final boolean isIdentical;

  public final boolean isMatch;

  public final double confidence;

  public final String faceId1;

  public final String docId1;

  public final String faceId1Url;

  public final String docId1Url;

  public KycMatchResponse(boolean success, boolean isIdentical, boolean isMatch, double confidence,
      String faceId1, String docId1, String faceId1Url, String docId1Url) {
    this.success = success;
    this.isIdentical = isIdentical;
    this.isMatch = isMatch;
    this.confidence = confidence;
    this.faceId1 = faceId1;
    this.docId1 = docId1;
    this.faceId1Url = faceId1Url;
    this.docId1Url = docId1Url;
  }
}
