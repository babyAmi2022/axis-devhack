package live.faceauth.sdk.gesture;

import com.google.android.gms.vision.face.Face;
import live.faceauth.sdk.R;

public class FinishTask extends GestureTask {

  @Override String getTaskName() {
    return FINISH_TASK;
  }

  @Override void updateFace(Face face) {

  }

  @Override public int getSuggestionDrawable() {
    return R.drawable.face_normal;
  }

  @Override boolean isLiveFace() {
    return false;
  }

  @Override int getHintMessageId(boolean tooSmall) {
    if (mFaceSmall) {
      return R.string.tracker_face_too_small;
    } else {
      return R.string.tracker_finishing;
    }
  }

  @Override void log() {

  }
}
